import networkx
import heapq
import math
import operator
import time
import sys
import os
from random import sample

def mergeSource(G,Iset):
    #  Input: Graph G and Set Source Iset
    #  Add new edge, give weights
    #  Return mergerd Graph and new Source
    I = 0
    G.add_node(I)
    G1 = G.copy()
    for node in Iset:
        #  for nbr in networkx.all_neighbors(G, node):
        # TODO: This Repeat
        for nbr in G.neighbors(node):
            w = G[node][nbr]['weight']
            # print node, nbr, w
            if G1.has_edge(I,nbr) == False:
                # print I, nbr, w
                G1.add_edge(I,nbr,weight = w)
            else:
                temp = G1[I][nbr]['weight'] + w;
                # print 'Add up ', I, nbr, temp
                G1[I][nbr]['weight'] = temp;
    G = G1.copy()
    for node in Iset:
        # print "Hey # ", node, "\n"
        if node in G:
            # print "Delete this \n"
            G.remove_node(node)
    if G.has_edge(I,I):
        G.remove_edge(I,I)
    return [G,I]

def db(G):
    print G.nodes()
    print G.edges()
    for (u,v,w) in G.edges(data = 'weight'):
        print u, v, w

class FibonacciHeap:
    #  internal node class
    class Node:
        def __init__(self, data):
            self.data = data
            self.parent = self.child = self.left = self.right = None
            self.degree = 0
            self.mark = False

    #  function to iterate through a doubly linked list
    def iterate(self, head):
        node = stop = head
        flag = False
        while True:
            if node == stop and flag is True:
                break
            elif node == stop:
                flag = True
            yield node
            node = node.right
    #  pointer to the head and minimum node in the root list
    root_list, min_node = None, None
    #  maintain total node count in full fibonacci heap
    total_nodes = 0
    #  return min node in O(1) time
    def find_min(self):
        return self.min_node

    #  extract (delete) the min node from the heap in O(log n) time
    #  amortized cost analysis can be found here (http://bit.ly/1ow1Clm)
    def extract_min(self):
        z = self.min_node
        if z is not None:
            if z.child is not None:
                #  attach child nodes to root list
                children = [x for x in self.iterate(z.child)]
                for i in xrange(0, len(children)):
                    self.merge_with_root_list(children[i])
                    children[i].parent = None
            self.remove_from_root_list(z)
            #  set new min node in heap
            if z == z.right:
                self.min_node = self.root_list = None
            else:
                self.min_node = z.right
                self.consolidate()
            self.total_nodes -= 1
        return z

    #  insert new node into the unordered root list in O(1) time
    def insert(self, data):
        n = self.Node(data)
        n.left = n.right = n
        self.merge_with_root_list(n)
        if self.min_node is None or n.data < self.min_node.data:
            self.min_node = n
        self.total_nodes += 1

    #  modify the data of some node in the heap in O(1) time
    def decrease_key(self, x, k):
        if k > x.data:
            return None
        x.data = k
        y = x.parent
        if y is not None and x.data < y.data:
            self.cut(x, y)
            self.cascading_cut(y)
        if x.data < self.min_node.data:
            self.min_node = x

    #  merge two fibonacci heaps in O(1) time by concatenating the root lists
    #  the root of the new root list becomes equal to the first list and the second
    #  list is simply appended to the end (then the proper min node is determined)
    def merge(self, h2):
        H = FibonacciHeap()
        H.root_list, H.min_node = self.root_list, self.min_node
        #  fix pointers when merging the two heaps
        last = h2.root_list.left
        h2.root_list.left = H.root_list.left
        H.root_list.left.right = h2.root_list
        H.root_list.left = last
        H.root_list.left.right = H.root_list
        #  update min node if needed
        if h2.min_node.data < H.min_node.data:
            H.min_node = h2.min_node
        #  update total nodes
        H.total_nodes = self.total_nodes + h2.total_nodes
        return H

    #  if a child node becomes smaller than its parent node we
    #  cut this child node off and bring it up to the root list
    def cut(self, x, y):
        self.remove_from_child_list(y, x)
        y.degree -= 1
        self.merge_with_root_list(x)
        x.parent = None
        x.mark = False

    #  cascading cut of parent node to obtain good time bounds
    def cascading_cut(self, y):
        z = y.parent
        if z is not None:
            if y.mark is False:
                y.mark = True
            else:
                self.cut(y, z)
                self.cascading_cut(z)

    #  combine root nodes of equal degree to consolidate the heap
    #  by creating a list of unordered binomial trees
    def consolidate(self):
        A = [None] * self.total_nodes
        nodes = [w for w in self.iterate(self.root_list)]
        for w in xrange(0, len(nodes)):
            x = nodes[w]
            d = x.degree
            while A[d] != None:
                y = A[d]
                if x.data > y.data:
                    temp = x
                    x, y = y, temp
                self.heap_link(y, x)
                A[d] = None
                d += 1
            A[d] = x
        #  find new min node - no need to reconstruct new root list below
        #  because root list was iteratively changing as we were moving
        #  nodes around in the above loop
        for i in xrange(0, len(A)):
            if A[i] is not None:
                if A[i].data < self.min_node.data:
                    self.min_node = A[i]

    #  actual linking of one node to another in the root list
    #  while also updating the child linked list
    def heap_link(self, y, x):
        self.remove_from_root_list(y)
        y.left = y.right = y
        self.merge_with_child_list(x, y)
        x.degree += 1
        y.parent = x
        y.mark = False

    #  merge a node with the doubly linked root list
    def merge_with_root_list(self, node):
        if self.root_list is None:
            self.root_list = node
        else:
            node.right = self.root_list.right
            node.left = self.root_list
            self.root_list.right.left = node
            self.root_list.right = node

    #  merge a node with the doubly linked child list of a root node
    def merge_with_child_list(self, parent, node):
        if parent.child is None:
            parent.child = node
        else:
            node.right = parent.child.right
            node.left = parent.child
            parent.child.right.left = node
            parent.child.right = node

    #  remove a node from the doubly linked root list
    def remove_from_root_list(self, node):
        if node == self.root_list:
            self.root_list = node.right
        node.left.right = node.right
        node.right.left = node.left

    #  remove a node from the doubly linked child list
    def remove_from_child_list(self, parent, node):
        if parent.child == parent.child.right:
            parent.child = None
        elif parent.child == node:
            parent.child = node.right
            node.right.parent = parent
        node.left.right = node.right
        node.right.left = node.left

def DFS(I, DAGGraph):
    degree = dict()
    visited = dict()
    for node in DAGGraph.nodes():
        degree[node] = 0
        visited[node] = False
    # print '# # # # # # # # # # # # # # # # # # '
    # db(DAGGraph)
    def dfs(u, prev):
        visited[u] = True
        for v in DAGGraph.neighbors(u):
            if v != prev and visited[v] == False:
                degree[v] = degree[u] + 1
                dfs(v, u)
    # db(DAGGraph)
    dfs(I, -1)
    return degree

def MIOA(I, G, theta):
    # print 'MIOA Graph'

    fixTheta = -math.log(theta)
    # print '# # # ' + str(fixTheta)
    Tree = networkx.DiGraph()
    fibHeap = FibonacciHeap()
    dis = dict()
    trace = dict()
    for node in G.nodes():
        dis[node] = float(100000000)
        trace[node] = -1
    dis[I] = 0
    fibHeap.insert((float(0), I))
    while fibHeap.total_nodes > 0:
        top = fibHeap.extract_min()
        disU = top.data[0]
        u = top.data[1]
        if abs(dis[u] - disU) > 0.00000001:
            continue
        # print 'U now is ' + str(u) + '-- and dis = ' + str(dis[u])
        for v in G.neighbors(u):
            w = G[u][v]['weight']
            w = -math.log(w)
            # print v, w
            # print 'Dis[' + str(v) + '] = ', dis[v]
            if dis[v] > dis[u] + w and dis[u] + w <= fixTheta:
                dis[v] = dis[u] + w
                # print 'Fix to ' + str(dis[v])
                trace[v] = u
                fibHeap.insert((dis[v],v))
    for node in G.nodes():
        if dis[node] > 0:
            if abs(dis[node]-100000000) < 0.00000001:
                dis[node] = 0
            else:
                # print 'Calculate exp ' + str(node) + '  ' + str(dis[node])
                dis[node] = math.exp(-dis[node])
    Tree.add_node(I)
    for node in G.nodes():
        if trace[node] != -1:
            Tree.add_weighted_edges_from([(trace[node], node, G[trace[node]][node]['weight'])])
    degree = DFS(I, Tree)
    return [Tree, dis, degree]

def DAG(g, Iset, theta):
    [G,I] = mergeSource(g, Iset)
    [DAG, dis, d] = MIOA(I,G,theta)
    for e in G.edges():
        u, v = e[0], e[1]
        if u in DAG.nodes() and v in DAG.nodes():
            if d[u] < d[v]:
                DAG.add_weighted_edges_from([(u, v, G[u][v]['weight'])])
    return [I, DAG]

def DAG2(G, I, theta):
    # print '$$$ DAG2 '
    # db(G)
    [DAG, dis, d] = MIOA(I,G,theta)
    for e in G.edges():
        u, v = e[0], e[1]
        if u in DAG.nodes() and v in DAG.nodes():
            if d[u] < d[v]:
                DAG.add_weighted_edges_from([(u, v, G[u][v]['weight'])])
    # print 'End DAG2'
    # db(G)
    return DAG

def TOPO(I, DAGGraph):
    ans = []
    visited = dict()
    fout = dict()
    for node in DAGGraph.nodes():
        visited[node] = False
        fout[node] = 1
    def dfsTOPO(u, prev):
        # print u, prev
        visited[u] = True
        for v in DAGGraph.neighbors(u):
            if v != prev :
                if visited[v] == False:
                    dfsTOPO(v, u)
                fout[u] += fout[v] * DAGGraph[u][v]['weight']
        ans.append(u)
    dfsTOPO(I, -1)
    # print "DFS Finist"
    ans = ans[::-1]
    # print ans
    # print fout
    return [ans, fout]

# Caculate the Spreading Role of all nodes
def EstInf(I, D):
    # ans = float(0)
    fin = dict()
    r = dict()
    for node in D.nodes():
        fin[node] = 0
    fin[I] = 1
    [topoList, fout] = TOPO(I, D)
    # print topoList
    for u in topoList:
        for v in D.neighbors(u):
            fin[v] += fin[u] * D[u][v]['weight']
        # print u, f[u]
        # ans += fin[u]
    for node in D.nodes():
        r[node] = fin[node] * fout[node]
    return r

def Update(G, Iset, theta, setU):
    visited = dict()
    for u in G.nodes():
        visited[u] = False
    newG = G.copy()
    for u in setU:
        if visited[u] == True:
            continue
        else:
            newG.remove_node(u)
            visited[u] = True
    answer = DAG(newG, Iset, theta)
    return answer

def Update2(G, I, theta, setU):
    # print 'Remove ' + str(u)
    visited = dict()
    for u in G.nodes():
        visited[u] = False
    newG = G.copy()
    for u in setU:
        if visited[u] == True:
            continue
        else:
            newG.remove_node(u)
            visited[u] = True
    # db(newG)
    answer = DAG2(newG, I, theta)
    # print 'End Update2'
    # db(answer)
    return answer

def depthD(G, I, d):
    def dfs(u):
        if u != I and depth[u] <= d:
            ans.append(u)
        for v in G.neighbors(u):
            if depth[v] == 0:
                depth[v] = depth[u] + 1
                dfs(v)
    depth = dict()
    for u in G.nodes():
        depth[u] = 0
    depth[I] = 1
    ans = []
    dfs(I)
    return ans

def H(G, I, theta, T):
    visited = dict()
    for u in G.nodes():
        visited[u] = False
    newG = G.copy()
    for u in T:
        if visited[u] == True:
            continue
        else:
            newG.remove_node(u)
            visited[u] = True
    D = DAG2(newG, I, theta)
    [answer,fin,fout] = EstInf(I, D)
    return answer

def HA(G, P, Theta, S, L, Costs):
    Gi = []
    Gcur = []
    Di = []
    A1 = []
    tempGraph = networkx.DiGraph()
    mergeGraph = networkx.DiGraph()
    newDAG = networkx.DiGraph()
    q = len(S)
    fun = 0.0
    sigma0 = 0.0
    cA1 = 0
    inDAG = dict()
    isSource = dict()
    for u in G.nodes():
        inDAG[u] = False
        isSource[u] = False
    for i in range(q):
        for u in S[i]:
            isSource[u] = True
    U = []
    # Line 1: Build q Graph Gi
    for i in range(q):
        #  Build Graph Gi' with new weight
        tempGraph.clear()
        tempGraph = G.copy()
        for (u,v,w) in tempGraph.edges(data = 'weight'):
            temp = tempGraph[u][v]['weight']
            tempGraph[u][v]['weight'] = temp * P[i][u]
        mergeGraph.clear()
        [mergeGraph, I] = mergeSource(tempGraph, S[i])
        # db(mergeGraph)
        Gi.append(mergeGraph.copy())
        Gcur.append(mergeGraph.copy())
        newDAG.clear()
        #  Line 2
        newDAG = DAG2(mergeGraph, I, Theta[i])
        # db(newDAG)
        Di.append(newDAG.copy())
        #  U
        for u in newDAG.nodes():
            if u != I and inDAG[u] == False and isSource[u] == False:
                U.append(u)
                inDAG[u] = True

    copyU = list(U)
    for u in copyU:
        if Costs[u] == 0:
            U.remove(u)
    # Line 5
    r = []
    for i in range(q):
        r.append(EstInf(0, Di[i]))
    vMax = 0
    sumMax = -1
    sumRu = 0
    for u in U:
        if Costs[u] > L:
            break
        sumRu = 0
        for i in range(q):
            if u in Di[i].nodes():
                sumRu += r[i][u]
        if sumRu > sumMax:
            sumMax = sumRu
            vMax = u
    # print vMax, sumMax
    rcMax = 0
    uMax = -1
    while True:
        cmin = 1000000000
        for u in U:
            if Costs[u] < cmin:
                cmin = Costs[u]
        if cmin + cA1 > L:
            break
        for i in range(q):
            Di[i].clear()
            Di[i] = DAG2(Gcur[i], I, Theta[i])
            r[i] = EstInf(I, Di[i])
        if len(U) == 0:
            break
        rcMax = -1
        uMax = -1
        for u in U:
            sumRu = 0
            if cA1 + Costs[u] <= L:
                for i in range(q):
                    if u in Di[i].nodes():
                        sumRu += r[i][u]
                if sumRu / Costs[u] > rcMax:
                    rcMax = sumRu / Costs[u]
                    uMax = u
            else:
                U.remove(u)
        if uMax in U:
            U.remove(uMax)
            # print "Remove ",uMax
            for i in range(q):
                # print "Graph ", i
                # db(Gcur[i])
                Gcur[i].remove_node(uMax)
            A1.append(uMax)
            cA1 += Costs[uMax]
    # Line 26
    # print A1
    sumA1 = 0
    for i in range(q):
        Di[i] = DAG2(Gcur[i], I, Theta[i])
        # print EstInf(I, Di[i])
        sumA1 += EstInf(I, Di[i])[I]

    # Line 27
    sumA2 = 0
    for i in range(q):
        Gi[i].remove_node(vMax)
        Di[i].clear()
        Di[i] = DAG2(Gi[i], I, Theta[i])
        sumA2 += EstInf(I, Di[i])[I]

    print sumA1, sumA2
    if sumA1 <= sumA2:
        return A1
    else:
        return [vMax]

def loadSourceNode(filename):
    S = []
    f = open(filename, 'r')
    lines = f.readlines()
    q = 0
    for line in lines:
        q += 1
        Si = []
        for sourceNode in line.split():
            Si.append(int(sourceNode))
        S.append(Si)
    f.close()
    return [S,q]

def loadEdges(filename):
    G = networkx.DiGraph()
    f = open(filename, 'r')
    # print 'What ?????????'
    lines = f.readlines()
    i = 0
    while i < len(lines):
        line = lines[i]
        [node1, node2, weight] = line.split()
        u = int(node1)
        v = int(node2)
        w = float(weight)
        G.add_weighted_edges_from([(u,v,w)])
        # G.add_weighted_edges_from([(v,u,w)])
        i += 1
    f.close()
    return G

def loadCost(filename):
    f = open(filename, 'r')
    lines = f.readlines()
    i = 0
    costs = dict()
    while i < len(lines):
        line = lines[i]
        [node, cost] = line.split()
        u = int(node)
        c = float(cost)
        costs[u] = c
        i += 1
    f.close()
    return costs

def loadBudgetAndTheta(filename):
    f = open(filename, 'r')
    lines = f.readlines()
    L = int(lines[0])
    oriTheta = lines[1].split()
    Theta = []
    for u in oriTheta:
        Theta.append(float(u))
    return [L, Theta]

def loadP(filename, n, q):
    f = open(filename, 'r')
    lines = f.readlines()
    id = 0
    P = []
    for i in range(q):
        pi = dict()
        for j in range(n):
            line = lines[id]
            [node, prob] = line.split()
            u = int(node)
            pij = float(prob)
            pi[u] = pij
            id += 1
        P.append(pi)
    return P

def outputResult(filename, R):
    f = open(filename, 'w')
    for u in R:
        f.write("%d\n"%u)
    f.close()

def outputTime(filename, start_time, end_time):
    f = open(filename, 'w')
    f.write(str(end_time-start_time))
    print 'Total time running is : ', str(end_time-start_time)
    f.close()

def degree(G, P, S, L, Costs):
    deg = sorted(dict(networkx.degree(G)).items(), key=lambda item: item[1])
    deg_nodes =[v for v,_ in deg][::-1]
    total_cost=0.0
    result=[]
    for node in deg_nodes:
        if total_cost + Costs[node] > L:
            break
        else:
            total_cost+=Costs[node]
            result.append(node)
    return result
            
def random(G, P, S, L, Costs):
    min_cost = min(Costs.values())
    nnum=L #int(L/min_cost)
    result = sample(G.nodes(), nnum)
    return result 


def main():
    data_name = str(sys.argv[1])
    budget_theta=sys.argv[2]
    alg=sys.argv[3]
    base_path = "../data/"
    path_to_file = base_path + data_name

    network_file = path_to_file + "/network.txt" 
    cost_file = path_to_file +"/cost.txt" 
    source_file = path_to_file + "/sources.txt"
    probability_file = path_to_file + "/"+"probability.txt"
    budget_theta_file = path_to_file+"/"+ budget_theta
    result_path= path_to_file + "/result/"+alg
    #print(probability_file)

    print 'Start :D'
    G = loadEdges(network_file)
    n = G.number_of_nodes()

    print 'Load Edge success'


    [S, q] = loadSourceNode(source_file)
    print 'Load Source success'

    Costs = loadCost(cost_file)
    print 'Load Costs sucess'

    [L, Theta] = loadBudgetAndTheta(budget_theta_file)
    print 'Load Budget and Theta success'

    P = loadP(probability_file, n, q)
    print 'Load Probability success'
    
    #print(random(G, P, S, L, Costs))
    Result=[]
    start_time = time.time()
    if alg=='random':
        Result= random(G, P, S, L, Costs)
    elif alg=='degree':
        Result= degree(G, P, S, L, Costs)
    else:
        Result =  HA(G, P, Theta, S, L, Costs)
    end_time = time.time()

    if not os.path.isdir(result_path):
        os.mkdir(result_path)

    file_result=result_path+"/r"+str(L)+".txt"
    file_time=result_path+"/t"+str(L)+".txt"
    outputResult(file_result, Result)
    outputTime(file_time, start_time, end_time)
    print('Result is: ')
    print Result
    print 'Program Ended'

if __name__=='__main__':
    main()
