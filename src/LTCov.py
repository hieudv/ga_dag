from collections import deque
import random
import networkx as nx
import sys

def read_source_node(file_source):
    source_set = []
    f=open(file_source,'r')
    lines=f.readlines()
    for line in lines:
        source_set.append(int(line))
        f.close()
    return source_set

def read_A_set(file_source):
    A_set = []
    f = open(file_source, 'r')
    lines = f.readlines()
    for line in lines:
        A_set.append(int(line))
        f.close()
    return A_set

def creat_directed_graph_LT(filename_graph, file_name_node):

    g = nx.DiGraph()
    f = open(filename_graph, 'r')
    lines = f.readlines()
    for line in lines:
        temp = line.split(" ")
        if len(temp)==3:
            index1 = int(temp[0])
            index2 = int(temp[1])
            weight = float(temp[2])
            g.add_edge(index1, index2, weight=weight)

    f.close()
    f_node=open(file_name_node,'r')
    for line_node in f_node:
        temp_node=line_node.split(' ')
        #print temp_node
        if len(temp_node)==2:
            node=int(temp_node[0])
            #print node
            cost=float(temp_node[1])
            #print cost
            g.node[node]['cost']=cost
    return g

def HA_LT(source_set,G, simulation_num, A):
    ha=LTCov(source_set,G,simulation_num,[])-LTCov(source_set,G,simulation_num,A)
    return ha

def LTCov(source_set,G, simulation_num, A):
    #G=nx.DiGraph()
    tol = 0.00001
    cov=0.0
    for node in A:
        if(node in G.nodes()):
            G.remove_node(node)
    for i in xrange(simulation_num):
        #print "Simulation: ", i
        cov1=1.0

        T=deque()
        Q=[]
        for s in source_set:
            for nbr in G.neighbors(s):
                if(nbr not in source_set):
                    if(nbr not in Q):
                        Q.append(nbr)
                        G.node[nbr]['active']=False
                        G.node[nbr]['in_weight']=G[s][nbr]['weight']
                        G.node[nbr]['threshold']=random.random()
                        T.append(nbr)
                    else:
                        G.node[nbr]['in_weight']+=G[s][nbr]['weight']
        while(len(T)):
            u=T.popleft()
            if((G.node[u]['active']==False) and (G.node[u]['in_weight']>=G.node[u]['threshold'])):
                G.node[u]['active']=True
                cov1=cov1+1
                #print "active node: ", u, "with threshold: ", G.node[u]['threshold'], "and in_weight", G.node[u]['in_weight'], G.node[u]['d']
                for nbr1 in G.neighbors(u):
                    if nbr1 in source_set: continue
                    if(nbr1 not in Q):
                        Q.append(nbr1)
                        G.node[nbr1]['active']=False
                        G.node[nbr1]['in_weight']=G[u][nbr1]['weight']
                        G.node[nbr1]['threshold']=random.random()
                        T.append(nbr1)
                    else:
                        if(G.node[nbr1]['active']==False):
                            T.append(nbr1)
                            G.node[nbr1]['in_weight']+=G[u][nbr1]['weight']

                            if(G.node[nbr1]['in_weight']-1>tol):
                                print "Something wrong, the inweight for a node is > 1. (nbr1, inweight) = ",nbr1,G.node[nbr1]['in_weight']-1
            #T.popleft()
        #print "num active node",cov1-1
        cov+=(float)(cov1/simulation_num)
    return cov

def main():
    edge_list=sys.argv[1]#"./dataset/graph.txt"
    cost_list=sys.argv[2]#"./dataset/cost.txt"
    source_file=sys.argv[3]
    A_file=sys.argv[4]
    simulation_num=(int)(sys.argv[5])
    G=creat_directed_graph_LT(edge_list,cost_list)
    source_set=read_source_node(source_file)
    A_set=read_A_set(A_file)
    #print "Number of edge: ", G.number_of_edges()
    #print "Number of nodes: ",G.number_of_nodes()
    #print G.edges(data=True)
    #print G.nodes(data=True)
    ha=HA_LT(source_set,G,simulation_num,A_set)
    print ha

if __name__ == '__main__':
    main()
