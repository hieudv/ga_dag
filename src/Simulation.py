from Graph import Graph, HyperGraph
#from numba import autojit, prange
import multiprocessing as mp
from joblib import Parallel, delayed
import time
import sys

class MoteCarlo(object):

    def __init__(self):
        pass

    def motecarlo_simulation(self, g,sources,num_simulation, hg, visit, visit_mark):
        #start=time.time()
        num_sources=len(sources)
        #print(sources)
        for s in sources:
            #print(s)
            visit[s]=True
        for i in range(num_sources):
            visit_mark[i]=sources[i]

        total_node=0
        for i in range(num_simulation):
            total_node+=hg.pollingLT1(g,sources,visit, visit_mark)
        
        total_node=total_node/num_simulation

        return total_node

    def simulation(self,g,sources, num_simulation,A):
        hg=HyperGraph()
        mt=MoteCarlo()
        nodeUs=g.nodes
        visit=[False]*(g.numNodes+1)
        visit_mark=[0]*(g.numNodes+1)
        for source in sources:
            visit[source]=True
            #nodeUs.remove(source)

        for a in A:
            visit[a]=True

        estimate=mt.motecarlo_simulation(g,sources,num_simulation, hg, visit, visit_mark)
        
        return estimate

    def read_source(self,file_name):
        f = open(file_name, 'r')
        data=f.readlines()
        sources=[]
        sources=[int(i.strip()) for i in data]
        return sources

    def loadSourceNode(self,filename):
        S = []
        f = open(filename, 'r')
        lines = f.readlines()
        q = 0
        for line in lines:
            q += 1
            Si = []
            for sourceNode in line.split():
                Si.append(int(sourceNode))
            S.extend(Si)
        f.close()
        return S

    def writetoFile(self,file_name,nodeList):
        f=open(file_name,'w')
        for node in nodeList:
            f.writelines(str(node)+"\n")
        f.close()

def main():

    data_name = str(sys.argv[1])
    #budget_theta=sys.argv[2]
    alg=sys.argv[2]
    nsimulation = int(sys.argv[3])
    base_path = "../data/"

    path_to_file = base_path + data_name

    network_file = path_to_file + "/network.txt" 
    cost_file = path_to_file +"/cost.txt" 
    source_file = path_to_file + "/sources.txt"
    probability_file = path_to_file + "/"+"probability.txt"
    attribute_file = path_to_file + "/attribute.txt"
    #budget_theta_file = path_to_file+"/"+ budget_theta
    result_path= path_to_file + "/result/"+alg

    
    #simulation_file = result_path+"/s"+str(L)+".txt"

    g=Graph()
    hg=HyperGraph()
    mt=MoteCarlo()

    g.readGraphLT(network_file,attribute_file)
    sources=mt.loadSourceNode(source_file)

    print("Number of nodes: "+ str(g.numNodes))
    print("Number of edges: "+ str(g.numEdges))

    
    es_arr=[]
    A=[]
    es_without_A=mt.simulation(g,sources,nsimulation,A)
    
    for L in range(5,105,5):
        file_result=result_path+"/r"+str(L)+".txt"
        simualtion_file = result_path+"/s"+str(L)+".txt"
        
        A=mt.read_source(file_result)
        es=mt.simulation(g,sources,nsimulation,A)
        es_result=es_without_A-es
        print(es_result)
        es_arr.append(es_result)
        es_arr.append(es_result)
        mt.writetoFile(simualtion_file,es_arr)
    # mt.writetoFile(outFile,es_arr)

    #si.process()
if __name__ == '__main__':
    main()
    


    