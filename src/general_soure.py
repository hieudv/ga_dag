import networkx as nx
from collections import deque
import random
from random import sample
import sys
#from collections import

def get_graph(data_file_name):
    graph=nx.read_edgelist(data_file_name, nodetype=int, create_using=nx.DiGraph())
    return graph

def get_in_degree(graph,node):
    #graph=nx.DiGraph()
    nei_in_list = []
    for in_edge in graph.in_edges(node):
        nei_in_list.append(in_edge[0])
    return nei_in_list

def gen_weight(graph, file_name):
    #graph=nx.DiGraph()
    for edge in graph.edges():
        graph[edge[0]][edge[1]]['weight']=(float)(1/(float)(len(get_in_degree(graph,edge[1]))))
    nx.write_weighted_edgelist(graph,file_name,encoding='utf-8')
    #return graph

def gen_source(graph, start, num_node):
    #for node in graph.nodes():
    queue = deque()
    queue.append(start)
    visited = []

    if start not in graph.nodes() or (len)(graph.neighbors(start)) == 0: return visited
    num=0
    while queue:
        node = queue.popleft()
        # print node
        if(len(visited)==num_node): return visited
        if node not in visited:
            visited.append(node)
            num+=1
            for nbr in graph.neighbors(node):
                if nbr not in queue:
                    queue.append(nbr)
                    # print queue
    return visited

def gen_source_v1(graph, file_name, nsources=3, nnodes=100):
    f=open(file_name,'w')
    sources =[]
    for i in range(nsources):
        source=sample(graph.nodes(), nnodes)
        sources.append(source)

    for s in sources:
        for node in s:
            f.write(str(node))
            f.write("\n")
    f.close()
    return sources

def gen_source_v2(graph, file_name, nsources=3, nnodes=100):
    f=open(file_name,'w')
    sources =[]
    for i in range(nsources):
        source=sample(graph.nodes(), nnodes)
        sources.append(source)

    for s in sources:
        for node in s:
            f.write(str(node))
            f.write(" ")
        f.write("\n")
    f.close()
    return sources 

def print_source_node(list,filename):
    f=open(filename,'wb')
    for element in list:
        f.write("%d\n"%element)
    f.close()

def get_cost(graph,file_name):
    tol_out_edge=0.0
    for node in graph.nodes():
        tol_out_edge+=(float)(len(graph.neighbors(node)))

    num_node=(float)(len(graph.nodes()))

    for node in graph.nodes():
        graph.node[node]['cost'] = random.uniform(1, 3)
    f=open(file_name,'wb')
    for node in graph.nodes():
        f.write("%d %f\n"%(node,graph.node[node]['cost']))
    f.close()

def get_ucost(graph,file_name):
    f=open(file_name,'wb')
    for node in graph.nodes():
        f.write("%d %d\n"%(node,1))
    f.close()

def get_max_degree(graph):
    max_degree=0
    tmp_node=0
    for node in graph.nodes():
        degree_node=len(graph.neighbors(node))
        if max_degree<degree_node:
            max_degree=degree_node
            tmp_node=node
    return [tmp_node,max_degree]

def gen_probality(graph,file_name):
    f = open(file_name, 'wb')
    for i in xrange(3):
        for node in graph.nodes():
            f.write("%d %f\n"%(node,random.uniform(0, 1)))
    f.close()

def main():
    data_name = str(sys.argv[1])
    base_path = "../data/"
    path_to_file = base_path + data_name

    network_file = path_to_file + "/network.txt" 
    cost_file = path_to_file +"/cost.txt"
    ucost_file = path_to_file +"/ucost.txt"

    source_file = path_to_file + "/sources.txt" 
    attribute_file = path_to_file +"/attribute.txt"
    probability_file = path_to_file +"/probability.txt"
    #theta_budget_file = path_to_file +"/"
    print("loading graph...")
    graph = get_graph(path_to_file+"/graph.txt")
    num_nodes = graph.number_of_nodes()
    num_edges = graph.number_of_edges()
    print("Load graph done!")
    
    # print("Loading weight...!")
    # gen_weight(graph, network_file)
    # print("Loaded weight has done!")

    print("Loading ucost...")
    get_ucost(graph, ucost_file)
    print("Load cost done!")
    
    # print("Load source node")
    #gen_source_v2(graph,)
    #print(graph.nodes())
    #print(gen_source_v2(graph, source_file))
    # f = open(attribute_file, 'w')
    # f.write("Number of nodes: " + str(num_nodes))
    # f.write("\n")
    # f.write("Number of edges: " + str(num_edges))
    # f.close()
    # print("Number of nodes: " + str(num_nodes))
    # print("Number of edges: " + str(num_edges))
    # print("Done!")

    #gen_probality(graph, probability_file)

    #print("probability done!")
    
if __name__ == '__main__':
    main()